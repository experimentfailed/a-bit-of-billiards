### License ###

This project is released under GPLv3:
https://www.gnu.org/licenses/gpl.html


### Description ###

This is a WiP. It is far from a complete game. This project is a
personal exercise to learn the Blender Game Engine. As such, I will be
very hesitant to accept any outside contributions other than art for now.


### TO-DO ###

* Finish dev ball set & triangle layouts
* Either fix pocket collisions or improve current detection...
* Multiplayer (simple local logic only for now; networking not planned)
* Implement rules (Scratch logic started; 8 / 9 ball game modes planned)
* HUD - player (who's turn is it), balls left, etc?
* Menu or buttons to start new game, change options, etc
* Aim cue stick X/Y against cue ball / implement spin & jump properly


### Known Issues ###

* Physics issues - sometimes balls get stuck in rails or exhibit
unwanted spin or direction. The later may now be fixed in the latest
commits. I've only had one instance of the former...

* Taking a shot - SHOULD BE FIXED - cue stick jumps in position when you hit
the button. You have to hit the button while the mouse is traveling backward,
for any hope...


### Controls & Gameplay (RMB = Right Mouse Button, etc) ###

* LMB - Adjust cue stick pitch
* MMB - Rotate cam & aim left/right
* RMB - Hold while dragging mouse to shoot (see above for bug)
* Scroll wheel - Zoom in out

Cue stick should disappear when any ball is in motion; reappearing at a
sane orientation against the cue ball when all balls have stopped.


### Requirements ###

Any Linux, Windows, or Mac OS X desktop that can run Blender.

Hardware recommendations to be determined. Certainly will be tame.

No binaries are available yet, so you will have to make your own by
installing Blender and exporting. Nevertheless, the aforementioned
platforms are planned for support.

NOTE: There may be, at any given time, more than 1 .blend file. The one
you want will always be "a_bit_of_billiards.blend". The others are
temporary, to create a save point prior to making potentially
destructive changes, and only slip through commits by accident
sometimes.


### Additional acknowledgements: ###

The classic http://www.billardgl.de/index-en.html for inspiration on
what to make as my first project, and some of the controls and gameplay
ideas. Really, it's just an overall benchmark for what I want to make.
It is also one of my all-time favorite games!
